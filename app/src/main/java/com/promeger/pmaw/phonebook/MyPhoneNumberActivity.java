package com.promeger.pmaw.phonebook;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MyPhoneNumberActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_phone_number);


        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        String phone = settings.getString("myPhoneNumber", "");

        if(phone.length() < 1)
        {
            phone = getString(R.string.editPNNow);
        }

        TextView tv = findViewById(R.id.myPhNum);
        tv.setText(phone);


        findViewById(R.id.myPhEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
            builder.setTitle(getString(R.string.edPhoneNDTitle));

            final EditText input = new EditText(view.getContext());
            input.setInputType(InputType.TYPE_CLASS_PHONE);

            input.setText(settings.getString("myPhoneNumber", ""));

            builder.setView(input);

            builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("myPhoneNumber", input.getText().toString());
                    editor.apply();

                    finish();
                    startActivity(getIntent());
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
            }
        });
    }
}
