package com.promeger.pmaw.phonebook;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


public class APIRequest extends AsyncTask<Void, Void, Void> {

    public APIRequest(PhoneBookService svc, URL url, HashMap<String,String> params) {
        this.invokingSvc = svc;
        this.apiUrl = url;
        this.apiParams = params;
        this.data = "";
    }

    @Override
    protected Void doInBackground(Void... arg0) {

        HttpURLConnection connection = null;

        try {
            connection = (HttpURLConnection)this.apiUrl.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            connection.connect();

            //Send
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
            wr.writeBytes(getPostDataString(this.apiParams));
            wr.flush();
            wr.close();

            //Response
            int response = connection.getResponseCode();

            if (response >= 400){
                this.data = "Connection Error";
            }
            else {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
               //String line = bufferedReader.lines().collect(Collectors.joining()); //API24
                String line = bufferedReader.readLine();
                this.data = line;

                if (BuildConfig.DEBUG) {
                    Log.d("APIRESPONSE", this.data.toString());
                }

            }


        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if(connection != null) {
                connection.disconnect();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {

        if(invokingSvc != null)
            invokingSvc.parseApiResponse(this.data);
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private PhoneBookService invokingSvc;
    private URL apiUrl;
    private HashMap<String, String> apiParams;

    private String data;
}
