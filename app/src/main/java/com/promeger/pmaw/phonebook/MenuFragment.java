package com.promeger.pmaw.phonebook;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class MenuFragment extends Fragment {


    public MenuFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_menu, container, false);
        ListView lV = v.findViewById(R.id.menuFragLV);

        List<String> items = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, items);
        lV.setAdapter(adapter);

        items.add(getString(R.string.myPhone));
        items.add(getString(R.string.phoneBook));
        items.add(getString(R.string.checkNumber));
        items.add(getString(R.string.Settings));

        lV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent in;
                switch (i) {
                    case 0:
                        in  = new Intent(getActivity(), MyPhoneNumberActivity.class);
                        startActivity(in);
                        break;
                    case  1:
                        in = new Intent(getActivity(), PhonebookAvtivity.class);
                        startActivity(in);
                        break;
                    case 2:
                        in = new Intent(getActivity(), CheckNumverActivity.class);
                        startActivity(in);
                        break;
                    case 3:
                        in = new Intent(getActivity(), SettingsActivity.class);
                        startActivity(in);
                        break;
                }
            }
        });

        return v;
    }

}
