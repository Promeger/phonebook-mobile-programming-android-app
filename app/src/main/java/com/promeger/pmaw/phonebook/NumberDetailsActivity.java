package com.promeger.pmaw.phonebook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.Intent;
import android.os.Bundle;

public class NumberDetailsActivity extends AppCompatActivity {


    public static boolean active = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = getIntent().getExtras();

        setContentView(R.layout.activity_number_details);
        b.putString("operation", "getData");
        Intent i = new Intent(this, NumberDetailsActivity.class);
        i.putExtras(b);
        i.setAction("svcQuery");
        LocalBroadcastManager.getInstance(this).sendBroadcast(i);
    }

    @Override
    protected void onPause() {
       active = false;
       super.onPause();
    }

    @Override
    protected void onResume() {
        active = true;
        super.onResume();
    }
}
