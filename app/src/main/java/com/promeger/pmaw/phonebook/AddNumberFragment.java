package com.promeger.pmaw.phonebook;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class AddNumberFragment extends Fragment {

    public AddNumberFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_number, container, false);

        Button b = v.findViewById(R.id.addPnBtn);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("operation", "add");
                b.putString("phone",  ((EditText)getActivity().findViewById(R.id.addPnEntry)).getText().toString());
                b.putString("name", ((EditText)getActivity().findViewById(R.id.addPnName)).getText().toString());

                Intent i = new Intent(getContext(), AddNumberActivity.class);
                i.putExtras(b);
                i.setAction("svcQuery");
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);
                getActivity().finish();
            }
        });
        return v;
    }

}
