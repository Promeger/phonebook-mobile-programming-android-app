package com.promeger.pmaw.phonebook;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


public class PhonebookFragment extends Fragment {

    private BroadcastReceiver recv;
    private ArrayList<PhonebookEntry> items = new ArrayList<>();
    PhonebookListItemAdapter adapter;

    public PhonebookFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        adapter = new PhonebookListItemAdapter(items, getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       final View v = inflater.inflate(R.layout.fragment_phonebook, container, false);

        final PhonebookFragment self = this;

        this.recv = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                self.items.add(new PhonebookEntry(intent.getExtras().getInt("key"), intent.getExtras().getString("name"), intent.getExtras().getString("phone"), null, getContext()));
                ListView lV = v.findViewById(R.id.pbFragLv);
                lV.setAdapter(adapter);

            };
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(recv, new IntentFilter("lista"));

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.items.clear();
        Bundle b = new Bundle();
        b.putString("operation", "list");
        Intent in = new Intent();
        in.putExtras(b);
        in.setAction("svcQuery");
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(in);
    }


    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(recv);
        super.onDestroy();
    }


}
