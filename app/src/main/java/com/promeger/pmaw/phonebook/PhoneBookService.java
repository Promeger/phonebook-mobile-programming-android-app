package com.promeger.pmaw.phonebook;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class PhoneBookService extends Service {

    private BroadcastReceiver recv;
    private DatabaseConnector conn;

    public PhoneBookService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        conn = new DatabaseConnector(this);
        final PhoneBookService self = this;
        recv = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String what = intent.getExtras().getString("operation");
                Bundle b = new Bundle();
                Intent in = new Intent();
                switch (what){
                    case "getData":
                        int idd = intent.getExtras().getInt("entryId");

                        HashMap<String, String> dta = conn.getContact(idd);

                        b.putString("phone",dta.get("phone"));
                        b.putString("name",dta.get("name"));
                        b.putInt("id",idd);
                        in.putExtras(b);
                        in.setAction("details");
                        LocalBroadcastManager.getInstance(context).sendBroadcast(in);
                        break;
                    case "checkNumber":
                        HashMap<String,String> params = new HashMap<>();
                        params.put("number", intent.getExtras().getString("number"));
                        try {
                            APIRequest req = new APIRequest(self, new URL("https://paiapi.promeger.com/api.php"), params);
                            req.execute();
                        }
                        catch (MalformedURLException e) {

                        }
                        break;
                    case "remove":
                        conn.removeEntry(intent.getExtras().getInt("entryId"));
                        break;
                    case "editName":
                        conn.updateName(intent.getExtras().getInt("entryId"), intent.getExtras().getString("name"));
                        break;
                    case "editPhone":
                        conn.updatePhone(intent.getExtras().getInt("entryId"), intent.getExtras().getString("phone"));
                        break;
                    case "add":
                        conn.insertEntry(intent.getExtras().getString("phone"), intent.getExtras().getString("name"));
                        break;
                    case "list":

                        ArrayList<Integer> ids = conn.getAllContactsIds();

                        for (Integer ajdi : ids) {
                            HashMap<String, String> dat = conn.getContact(ajdi);
                            Bundle bund = new Bundle();
                            Intent inte = new Intent();
                            bund.putString("phone",dat.get("phone"));
                            bund.putString("name",dat.get("name"));
                            bund.putInt("key",ajdi);
                            inte.putExtras(bund);
                            inte.setAction("lista");
                            LocalBroadcastManager.getInstance(context).sendBroadcast(inte);
                        }
                        break;
                }
            };
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(recv, new IntentFilter("svcQuery"));

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(recv);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


    public void parseApiResponse(String data) {
        Bundle bund = new Bundle();
        Intent inte = new Intent();
        bund.putString("country",data);
        inte.putExtras(bund);
        inte.setAction("numberResponse");
        LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(inte);
    }

}
