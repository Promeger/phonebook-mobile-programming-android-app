package com.promeger.pmaw.phonebook;


import android.content.Context;
import android.widget.ImageView;

import androidx.annotation.Nullable;


public class PhonebookEntry {

    public PhonebookEntry(int id, String title, String phone, @Nullable ImageView img, Context ctx) {
        this.id = id;
        this.title = title;
        this.phone = phone;
        this.img = new ImageView(ctx);

        if(img != null)
            this.img = img;
        else
            this.img.setImageResource(R.drawable.ic_person);
    }

    int id;
    String title;
    String phone;
    ImageView img;
}
