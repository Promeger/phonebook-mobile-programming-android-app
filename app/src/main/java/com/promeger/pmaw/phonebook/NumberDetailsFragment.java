package com.promeger.pmaw.phonebook;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.app.Fragment;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;


public class NumberDetailsFragment extends Fragment {

    private BroadcastReceiver detailsBroadcastRecv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final NumberDetailsFragment self = this;
        detailsBroadcastRecv = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ((TextView)getActivity().findViewById(R.id.detPhone)).setText(intent.getExtras().getString("phone"));
                ((TextView)getActivity().findViewById(R.id.detName)).setText(intent.getExtras().getString("name"));
                self.idd = intent.getExtras().getInt("id");
            };
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(detailsBroadcastRecv, new IntentFilter("details"));
        final NumberDetailsFragment self = this;
        View v = inflater.inflate(R.layout.fragment_number_details, container, false);

        v.findViewById(R.id.detBEdNa).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle(getString(R.string.edPhoneNDTitle));

                final EditText input = new EditText(view.getContext());
                input.setInputType(InputType.TYPE_CLASS_TEXT);

                input.setText(((TextView)getActivity().findViewById(R.id.detName)).getText());

                builder.setView(input);

                builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                //Send Update
                Bundle b = new Bundle();
                b.putString("operation", "editName");
                b.putInt("entryId", self.idd);
                b.putString("name", input.getText().toString());
                Intent in = new Intent();
                in.putExtras(b);
                in.setAction("svcQuery");
                LocalBroadcastManager.getInstance(self.getContext()).sendBroadcast(in);


                self.getActivity().finish();
                startActivity(self.getActivity().getIntent());
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        v.findViewById(R.id.detBEdPh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle(getString(R.string.edPhoneNDTitle));

                final EditText input = new EditText(view.getContext());
                input.setInputType(InputType.TYPE_CLASS_PHONE);

                input.setText(((TextView)getActivity().findViewById(R.id.detPhone)).getText());

                builder.setView(input);

                builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                //Send Update
                Bundle b = new Bundle();
                b.putString("operation", "editPhone");
                b.putInt("entryId", self.idd);
                b.putString("phone", input.getText().toString());
                Intent in = new Intent();
                in.putExtras(b);
                in.setAction("svcQuery");
                LocalBroadcastManager.getInstance(self.getContext()).sendBroadcast(in);

                self.getActivity().finish();
                startActivity(self.getActivity().getIntent());
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        v.findViewById(R.id.detBDel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("operation", "remove");
                b.putInt("entryId", self.idd);
                Intent in = new Intent();
                in.putExtras(b);
                in.setAction("svcQuery");
                LocalBroadcastManager.getInstance(self.getContext()).sendBroadcast(in);

                self.getActivity().finish();
            }
        });

        return  v;
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(detailsBroadcastRecv);
        super.onDestroy();
    }

    public int idd = -1;
}
