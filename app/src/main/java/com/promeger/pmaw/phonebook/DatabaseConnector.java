package com.promeger.pmaw.phonebook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseConnector extends SQLiteOpenHelper {

    public DatabaseConnector(Context ctx) {
        super(ctx, "pbook.db", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Phonebook (id INTEGER PRIMARY KEY, name TEXT, phone TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {

    }

    public void insertEntry(String phone, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        db.insert("Phonebook", null, contentValues);
    }

    public void updateName(int id, String newName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", newName);
        db.update("Phonebook", contentValues, "id = ?", new String[] { Integer.toString(id) });
    }

    public void updatePhone(int id, String newPhone) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("phone", newPhone);
        db.update("Phonebook", contentValues, "id = ?", new String[] { Integer.toString(id) });
    }

    public void removeEntry(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("Phonebook","id = ? ", new String[] { Integer.toString(id) });
    }

    public ArrayList<Integer> getAllContactsIds() {
        ArrayList<Integer> toRet = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT id FROM Phonebook", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            toRet.add(res.getInt(res.getColumnIndex("id")));
            res.moveToNext();
        }
        return toRet;
    }

    public HashMap<String, String> getContact(int contactId) {
        HashMap<String,String> toRet = new HashMap<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT name, phone FROM Phonebook WHERE id=?", new String[]{Integer.toString(contactId)} );
        res.moveToFirst();

        toRet.put("name", res.getString(res.getColumnIndex("name")));
        toRet.put("phone", res.getString(res.getColumnIndex("phone")));
        return toRet;

    }

}
