package com.promeger.pmaw.phonebook;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class PhonebookListItemAdapter extends ArrayAdapter<PhonebookEntry> implements View.OnClickListener {

    private static class Helper {
        TextView txtName;
        TextView txtPhone;
        ImageView img;
        int position;
    }

    private int lastPosition = -1;

    public PhonebookListItemAdapter(ArrayList<PhonebookEntry> data, Context context) {
        super(context, R.layout.phonebook_listitem, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=((Helper)v.getTag()).position;
        Object object= getItem(position);
        PhonebookEntry dataModel=(PhonebookEntry) object;

        Bundle b = new Bundle();
        b.putInt("entryId", dataModel.id);
        b.putString("operation", "getData");

        Intent i = new Intent(v.getContext(), NumberDetailsActivity.class);
        i.putExtras(b);
        i.setAction("svcQuery");

        //Start Activity if not running
        if(!NumberDetailsActivity.active)
            v.getContext().startActivity(i);
        else {
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(i);

        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PhonebookEntry dataModel = getItem(position);
        Helper viewHolder;


        if (convertView == null) {
            viewHolder = new Helper();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.phonebook_listitem, parent, false);
            viewHolder.txtName = convertView.findViewById(R.id.pbLiNa);
            viewHolder.txtPhone = (convertView.findViewById(R.id.pbLiPn));
            viewHolder.img =  convertView.findViewById(R.id.pbLiIV);
            viewHolder.position = position;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (Helper) convertView.getTag();
        }

        convertView.setOnClickListener(this);
        lastPosition = position;

        viewHolder.txtName.setText(dataModel.title);
        viewHolder.txtPhone.setText(dataModel.phone);
        viewHolder.img = dataModel.img;

        return convertView;
    }

    private ArrayList<PhonebookEntry> dataSet;
    Context mContext;
}
