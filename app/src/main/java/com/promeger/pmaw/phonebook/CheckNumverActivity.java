package com.promeger.pmaw.phonebook;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CheckNumverActivity extends AppCompatActivity {

    private BroadcastReceiver recv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_numver);

        recv = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                new AlertDialog.Builder(CheckNumverActivity.this)
                        .setTitle(R.string.country)
                        .setMessage(getString(R.string.thisnum) + intent.getExtras().getString("country"))
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                    CheckNumverActivity.this.finish();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show();


            };
        };

        findViewById(R.id.chBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               EditText et = findViewById(R.id.chInput);

                Bundle b = new Bundle();
                b.putString("operation", "checkNumber");
                b.putString("number", et.getText().toString());
                Intent in = new Intent();
                in.putExtras(b);
                in.setAction("svcQuery");
                LocalBroadcastManager.getInstance(view.getContext()).sendBroadcast(in);
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(recv, new IntentFilter("numberResponse"));
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(recv);
        super.onDestroy();

    }
}
